<?php

namespace App\Http\Controllers;

use App\Models\Ability;
use App\Models\Package;
use App\Models\Role;
use App\Models\User;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

class AidarController extends Controller
{
    public function users()
    {
        $users = User::all()->sortBy('id');

//        return  dd($users[0]->roles[0]->name);
        foreach ($users as $user){
            $user->role=$user->roles[0];

            return $user->roles[0];

        }

        return $users;
    }

    public function packageServices()
    {
        $packages = Package::all();

        foreach ($packages as $package){
            $package = [
                'package'=>$package,
                'services' => $package->services];
        }

        return $packages;
    }

    public function createAdmin(){
        $user = User::find(1);

        $role = Role::firstOrCreate([
            'name'=>'administrator'
        ]);

        $ability = Ability::firstOrCreate([
            'name'=>'admin'
        ]);

        $role->allowTo($ability);
        $user->assignRole($role);

        return $user;
//        return $user->abilities();
//        $roles = $user->roles();
//        $abilitiesOfRole = $roles->abilities();
    }


    public function createModerator(){
        $user = User::find(1);

        $role->allowTo($ability);

        $role = Role::firstOrCreate([
            'name'=>'moderator'
        ]);

        $ability = Ability::firstOrCreate([
            'name'=>'view_request'
        ]);

        $user->assignRole($role);
        $role->allowTo($ability);

        return $user;
    }

    public function createContentManager(){
        $user = User::find(1);

        $role = Role::firstOrCreate([
            'name'=>'content_manager'
        ]);

        $ability = Ability::firstOrCreate([
            'name'=>'edit_content'
        ]);

        $user->assignRole($role);
        $role->allowTo($ability);

        return $user;
    }

    public function home(Filesystem $file){
        return View::make('home');

//        return $file->get(public_path('robots.txt'));
//        return File::get(public_path('robots.txt'));
  }

}
