<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use App\Notifications\RequestReceived;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\View;
use App\Models\Request;

class ContactController extends Controller
{
    public function send()
    {
        request()->validate([
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
        ]);

        $data = [
            'email' => request('email'),
            'phone' => request('phone'),
            'name' => request('name')
        ];

        Mail::to(\request('email'))
            ->send(new Contact($data));

        Request::create([
            'name' => request('name'),
            'phone' => request('phone'),
            'email' => request('email'),
        ]);

        return redirect('/');
    }
}
