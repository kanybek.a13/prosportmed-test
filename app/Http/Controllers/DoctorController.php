<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    //
    public function index(){
        return view('app.doctor.index', [
            'doctors' => Doctor::all()->sortBy('id')
        ]);
    }

    public function show(Doctor $doctor){
        return view('app.doctor.show',[
            'doctor'=>$doctor
        ]);
    }
}
