<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Equipment;

class EquipmentController extends Controller
{
    //
    public function index(){
        return view('app.equipment.index',[
            'equipments' => Equipment::all()->sortBy('id')
        ]);
    }

    public function show(Equipment $equipment){
        return view('app.equipment.show',[
            'equipment' => $equipment
        ]);
    }
}
