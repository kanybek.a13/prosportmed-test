<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use App\Models\Service;

class ServiceController extends Controller
{
    public function index(){
        $packages = Package::all()->sortBy('id');

        foreach ($packages as $package){
            $package = [
                'package'=>$package,
                'services' => $package->services
            ];
        }

        return view('app.service.index', [
            'services' => Service::all(),
            'packages' => $packages
        ]);
    }

    public function show(Service $service){
        return view('app.service.show',[
            'service'=>$service
        ]);
    }
}
