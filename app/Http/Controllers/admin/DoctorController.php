<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctor;

class DoctorController extends Controller
{
    public function index(){
        return view('admin.doctor.index', [
            'doctors' => Doctor::all()->sortBy('id')
        ]);
    }

    public function show(Doctor $doctor){
        return view('admin.doctor.show', [
            'doctor'=> $doctor
        ]);
    }

    public function edit(Doctor $doctor){
        return view('admin.doctor.edit', [
            'doctor'=> $doctor
        ]);
    }

    public function update(){
        $validatedData = $this->validateDoctorInfo(\request());
//        return dd($validatedData);
        $doctor = Doctor::findOrFail(\request('id'));

        $doctor->name = $validatedData['name'];
        $doctor->body = $validatedData['body'];
        $doctor->position = $validatedData['position'];

        if (\request('image')) {
            $doctor->image = request('image');
        }else
            $doctor->image = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9JDd1zjxV28dMxnmr6QddwLJC1AIephsk-Q&usqp=CAU';

        $doctor->save();

        return redirect('/admin/doctors');
    }

    public function delete(Doctor $doctor){
        $doctor->delete();

        return redirect('/admin/doctors');
    }

    public function new(){
        return view('admin.doctor.new');
    }

    public function store(){
        $validatedData = $this->validateDoctorInfo(\request());

        $doctor = new Doctor();

        $doctor->name = $validatedData['name'];
        $doctor->body = $validatedData['body'];
        $doctor->position = $validatedData['position'];

        if (\request('image')){
            $doctor->image = request('image');
        }else
            $doctor->image = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9JDd1zjxV28dMxnmr6QddwLJC1AIephsk-Q&usqp=CAU';

        $doctor->save();

        return redirect('/admin/doctors');
    }

    public function validateDoctorInfo(){
        return request()->validate([
            'name'=>'required',
            'position'=>'required',
            'body'=>'required',
        ]);
    }
}


