<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Equipment;

class EquipmentController extends Controller
{
    public function index(){
        return view('admin.equipment.index', [
            'equipments' => Equipment::all()->sortBy('id')
        ]);
    }

    public function show(Equipment $equipment){
        return view('admin.equipment.show', [
            'equipment'=> $equipment
        ]);
    }

    public function edit(Equipment $equipment){
        return view('admin.equipment.edit',[
            'equipment' => $equipment
        ]);
    }

    public function new(){
        return view('admin.equipment.new');
    }

    public function update(Equipment $equipment){
        $validatedData = $this->validateEquipmentInfo(\request());

        $equipment = Equipment::findOrFail(\request('id'));

        $equipment->name = $validatedData['name'];
        $equipment->description = $validatedData['description'];
        $equipment->price = $validatedData['price'];

        if (\request('image')) {
            $equipment->image = \request('image');
        }else
            $equipment->image = 'https://st2.depositphotos.com/1028911/7301/i/600/depositphotos_73011163-stock-photo-various-medical-equipment-isolated-on.jpg';

        $equipment->save();

        return redirect('/admin/equipments');
    }

    public function store(){
        $validatedData = $this->validateEquipmentInfo(\request());

        $equipment = new Equipment;

        $equipment->name = $validatedData['name'];
        $equipment->description = $validatedData['description'];
        $equipment->price = $validatedData['price'];

        if (\request('image')){
            $equipment->image = \request('image');
        }else
            $equipment->image = 'https://st2.depositphotos.com/1028911/7301/i/600/depositphotos_73011163-stock-photo-various-medical-equipment-isolated-on.jpg';

        $equipment->save();

        return redirect('/admin/equipments');
    }

    public function delete(Equipment $equipment){
        $equipment->delete();

        return redirect('/admin/equipments');
    }

    public function validateEquipmentInfo($data){
        return request()->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);
    }
}
