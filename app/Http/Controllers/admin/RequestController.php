<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Request;

class RequestController extends Controller
{
    public function index(){
        return view('admin.request.index', [
            'requests' => Request::all()->sortBy('id')
        ]);
    }

    public function show(Request $request){
        return view('admin.doctor.show', [
            'request'=> $request
        ]);
    }

    public function edit(Request $request){
        return view('admin.doctor.edit', [
            'request'=> $request
        ]);
    }

    public function delete(Request $request){
        $request->delete();

        return redirect('/admin/requests');
    }
}
