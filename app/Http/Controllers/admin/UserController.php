<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        $users = User::all()->sortBy('id');

//        return  dd($users[0]->roles[0]->name);
        foreach ($users as $user){
            $user->role=$user->roles[0];
        }

        return view('admin.user.index', [
            'users' => $users
        ]);
    }

    public function show(User $user){
        return view('admin.user.show', [
            'user' => $user,
            'user_role' =>  $user->roles[0]
        ]);
    }

    public function edit(User $user){
        return view('admin.user.edit', [
            'user'=> $user,
            'user_role' => $user->roles[0],
            'roles' => Role::all()->sortBy('name')
        ]);
    }

    public function update(){
        $validatedData = $this->validateUserInfo(\request());
        $user = User::findOrFail(\request('id'));

        $user->name = $validatedData['name'];
        $user->email = $validatedData['email'];

        //doesn't work
        $user->assignRole($validatedData['role']);

        $user->save();

        return redirect('/admin/users');
    }

    public function delete(User $user){
        $user->delete();

        return redirect('/admin/users');
    }

    public function new(){
        return view('admin.user.new',[
            'roles' => Role::all()->sortBy('name')
        ]);
    }

    public function store(){
//doesn't work

//        $validatedData = $this->validateUserInfo(\request());
//
//        $user = new User();
//
//        $user->name = $validatedData['name'];
//        $user->email = $validatedData['email'];
//        $user->password = 'password';
//        $user->assignRole($validatedData('role'));
//
//        $user->save();
//
        return redirect('/admin/users');
    }

    public function validateUserInfo(){
        return request()->validate([
            'name'=>'required',
            'email'=>'required|email',
            'role'=>'required',
        ]);
    }
}
