<?php

namespace Database\Factories;

use App\Models\Doctor;
use Illuminate\Database\Eloquent\Factories\Factory;

class DoctorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Doctor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
//    public function definition()
//    {

     $factory->define(Doctor::class, function (Faker $faker){
        return [
            'name' => $faker->name,
            'position' => 'доктор',
            'title' => $faker->sentence(15),
            'body' => $faker->text(100), // password
        ];
    });
}
