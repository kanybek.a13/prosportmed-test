<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('doctors')->insert([
//            'name' => Str::random(10),
//            'position' => 'врач',
//            'body' =>  Str::random(50),
//        ]);

        $faker = Faker::create();
        foreach (range(1,10) as $index) {

            DB::table('doctors')->insert([
                'name' => $faker->name,
                'position' => 'врач',
                'body' =>  $faker->text,
            ]);
        }
    }
}
