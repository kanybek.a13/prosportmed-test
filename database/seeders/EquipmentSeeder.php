<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EquipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index) {

            DB::table('equipments')->insert([
                'name' => $faker->text,
                'price' => '100000',
                'description' =>  $faker->text,
            ]);
        }    }
}
