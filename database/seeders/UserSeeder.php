<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin1',
            'email' => 'admin1@gmail.com',
            'password' => Hash::make('admin1'),
        ]);

        DB::table('users')->insert([
            'name' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => Hash::make('admin2'),
        ]);

        DB::table('users')->insert([
            'name' => 'content_manager1',
            'email' => 'content_manager1@gmail.com',
            'password' => Hash::make('content_manager1'),
        ]);

        DB::table('users')->insert([
            'name' => 'content_manager2',
            'email' => 'content_manager2@gmail.com',
            'password' => Hash::make('content_manager2'),
        ]);

        DB::table('users')->insert([
            'name' => 'moderator2',
            'email' => 'moderator2@gmail.com',
            'password' => Hash::make('moderator2'),
        ]);

        DB::table('users')->insert([
            'name' => 'moderator1',
            'email' => 'moderator1@gmail.com',
            'password' => Hash::make('moderator1'),
        ]);
    }
}
