@include('admin.layouts.header')
@can('edit_content')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="#" title="Доктор">Доктор</a></li>
        <li><span>{{$doctor->name}}</span></li>
    </ul>

    <form class="block" method="post" action="/admin/doctor">
        @csrf
        @method('put')

        <input type="hidden" name="id" value="{{$doctor->id}}">

        <div class="tabs-contents">
            <div class="active">

                <div class="input-group">
                    <div class="col-md-2 col-sm-2">
                        <img src="{{$doctor->image}}" alt="" width="500">
                    </div>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> ФИО</label>
                    <input type="text" name="name" value="{{$doctor->name}}" placeholder="ФИО" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Должность</label>
                    <input type="text" name="position" value="{{$doctor->position}}" placeholder="Должность" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Описание</label>
                    <textarea name="body" placeholder="Описание" class="input-regular">{{$doctor->body}}
                    </textarea>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Путь к фото</label>
                    <input type="text" name="image" value="{{$doctor->image}}" placeholder="Путь к фото" class="input-regular">
                </div>
                <br>
{{--                <div class="input-group">--}}
{{--                    <label class="input-group__title">Инструкция по работе с фондом</label>--}}
{{--                    <input type="hidden" name="instruction_document"--}}
{{--                           value="">--}}
{{--                    <div id="file1" class="file-upload">--}}
{{--                        <div id="file1_uploader" class="file">--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar"></div>--}}
{{--                            </div>--}}
{{--                            <span class="file__name">--}}
{{--                                            .docx, .doc, .xls, .xlsx, .pdf, .txt, .ppt, .pptx • 25 MB<br/>--}}
{{--                                            <strong>Загрузить документ</strong>--}}
{{--                                        </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
            <div>
                <button class="btn btn--red"><a href="/admin/doctor/delete/{{$doctor->id}}" title="Удалить"></a>Удалить</button>
            </div>
        </div>
    </form>
</div>

</main>
@endcan
@include('admin.layouts.footer');
