@include('admin.layouts.header')
@can('edit_content')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="#" title="Оборудование">Оборудование</a></li>
        <li><span>{{$equipment->name}}</span></li>
    </ul>

    <form class="block" method="post" action="/admin/equipment">
        @csrf
        @method('put')

        <input type="hidden" name="id" value="{{$equipment->id}}">

        <div class="tabs-contents">
            <div class="active">

                <div class="input-group">
                    <div class="col-md-4 col-sm-6">
                        <img src="{{$equipment->image}}" alt="" width="500">
                    </div>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Наименование оборудование</label>
                    <input type="text" name="name" value="{{$equipment->name}}" placeholder="Название" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Описание</label>
                    <textarea name="description" placeholder="Описание" class="input-regular">{{$equipment->description}}
                    </textarea>
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Цена</label>
                    <input type="number" name="price" value="{{$equipment->price}}" placeholder="Цена" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Путь к фото</label>
                    <input type="text" name="image" value="{{$equipment->image}}" placeholder="Путь к фото" class="input-regular">
                </div>
                <br>
{{--                <div class="input-group">--}}
{{--                    <label class="input-group__title">Инструкция по работе с фондом</label>--}}
{{--                    <input type="hidden" name="instruction_document"--}}
{{--                           value="">--}}
{{--                    <div id="file1" class="file-upload">--}}
{{--                        <div id="file1_uploader" class="file">--}}
{{--                            <div class="progress">--}}
{{--                                <div class="progress-bar"></div>--}}
{{--                            </div>--}}
{{--                            <span class="file__name">--}}
{{--                                            .docx, .doc, .xls, .xlsx, .pdf, .txt, .ppt, .pptx • 25 MB<br/>--}}
{{--                                            <strong>Загрузить документ</strong>--}}
{{--                                        </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
            <div>
                <button class="btn btn--red"><a href="/admin/equipment/delete/{{$equipment->id}}" title="Удалить"></a>Удалить</button>
            </div>
        </div>
    </form>
</div>

</main>

@endcan
@include('admin.layouts.footer');
