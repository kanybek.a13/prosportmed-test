@include('admin.layouts.header')
@can('edit_content')

        <main class="main">
            <div class="container container-fluid">
                <div class="title-block">
                    <div class="row row--multiline align-items-center">
                        <div class="col-md-4">
                            <h1 class="title-primary" style="margin-bottom: 0">Спиок оборудовании</оборудовании></h1>
                        </div>
                        <div class="col-md-8 text-right-md text-right-lg">
                            <div class="flex-form">
                                <div>
                                    <a href="#" title="Расширенный поиск" class="btn"><i class="icon-search"></i> <span>Расширенный поиск</span></a>
                                </div>
                                <div>
                                    <form class="input-button">
                                        <input type="text" name="search" placeholder="Наименование фонда/Номер фонда" class="input-regular input-regular--solid" style="width: 282px;">
                                        <button class="btn btn--green">Найти</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <h2 class="title-secondary">Список врачей</h2>

                    <table class="table records">
                        <colgroup>
                            <col span="1" style="width: 3%;">
                            <col span="1" style="width: 20%;">
                            <col span="1" style="width: 12%;">
                            <col span="1" style="width: 20%;">
                            <col span="1" style="width: 15%;">
                            <col span="1" style="width: 15%;">
                            <col span="1" style="width: 15%;">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Название</th>
                            <th>Цена</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($equipments as $equipment)
                            <tr>
                                <td>{{$equipment->id}}</td>
                                <td>{{$equipment->name}}</td>
                                <td>{{$equipment->price}}</td>
                                <td>
                                    <div class="action-buttons">
                                        <a href="/admin/equipment/{{$equipment->id}}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                        <a href="/admin/equipment/edit/{{$equipment->id}}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                        <a href="/admin/equipment/delete/{{$equipment->id}}" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </main>

@endcan
@include('admin.layouts.footer');
