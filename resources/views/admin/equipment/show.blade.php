@include('admin.layouts.header')
@can('edit_content')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="#" title="Оборудование">Оборудование</a></li>
        <li><span>{{$equipment->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$equipment->id}}</div>
            <h1 class="fund-header__title">{{$equipment->name}}</h1>
{{--            <div class="fund-header__organization">Туркестанский областной государственный архив</div>--}}
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$equipment->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения	</div>
                <div class="property__text">{{$equipment->updated_at}}<br>
                    Администратор Panama DC</div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">

            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                </div>
            </div>

            <div class="tabs-contents">
                <div class="active">

                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <img src="{{$equipment->image}}" alt="" width="500">
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <a href="/admin/equipment/edit/{{$equipment->id}}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Название</label>
                        <input type="text" name="name" value="{{$equipment->name}}" placeholder="Название" class="input-regular" disabled>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Описание</label>
                        <textarea name="description" placeholder="{{$equipment->description}}" class="input-regular" disabled></textarea>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Цена</label>
                        <input type="number" name="price" value="{{$equipment->price}}" placeholder="Цена" class="input-regular" disabled>
                    </div>
            </div>
        </div>
    </div>
</div>

</main>

@endcan
@include('admin.layouts.footer');
