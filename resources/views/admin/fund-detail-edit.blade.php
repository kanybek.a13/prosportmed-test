<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Flowdoc</title>

    <link rel="stylesheet" href="/admin/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/chosen/chosen.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/air-datepicker/dist/css/datepicker.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/css/style.css" type="text/css" media="screen"/>

    <link rel="apple-touch-icon" sizes="180x180" href="/admin/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/admin/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/admin/favicon/favicon-16x16.png">
    <link rel="manifest" href="/admin/favicon/site.webmanifest">
    <link rel="mask-icon" href="/admin/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <meta name="title" content="Заголовок">
    <meta name="description" content="Описание">
    <meta name="keywords" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Заголовок">
    <meta property="og:description" content="Описание">
    <meta property="og:url" content="/">
    <meta property="og:image" content="/admin/img/og-image.jpg">
    <meta property="og:site_name" content="localhost:9876">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Заголовок">
    <meta property="twitter:description" content="Описание">
    <meta property="twitter:image" content="/admin/img/og-image.jpg">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <meta property="twitter:url" content="/">
    <meta property="author" content="">
    <meta name="relap-image" content="/admin/img/og-image.jpg">
    <meta name="relap-title" content="Заголовок">
    <meta name="relap-description" content=" ">

</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
<aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/" title="Главная" class="logo"><img src="/admin/img/logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            <li class="dropdown active">
                <a href="javascript:;" title="Архив"><i class="icon-archive"></i> Архив</a>
                <ul>
                    <li><a href="#" title="Список фондов">Список фондов</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                    <li><a href="#" title="Список дел">Список дел</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Отчеты"><i class="icon-reports"></i> Отчеты</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Справочник"><i class="icon-directory"></i> Справочник</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Организации"><i class="icon-organizations"></i> Организации</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Пользователи"><i class="icon-users"></i> Пользователи</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
<div class="right-wrapper">
    <header class="header">
        <div class="container container-fluid">
            <a href="javascript:;" title="Свернуть/развернуть навигацию" class="menu-btn icon-menu"></a>
            <a href="/" title="Главная" class="logo hidden-md hidden-lg"><img src="/admin/img/logo-blue.svg" alt=""></a>
            <div class="language hidden-sm hidden-xs">
                <a href="#" title="РУС" class="active">РУС</a>
                <a href="#" title="ENG">ENG</a>
            </div>
            <div class="header-dropdown account-nav">
                <div class="header-dropdown__title">
                    <span>Добро пожаловать, Илья!</span> <img src="/admin/img/user.svg" alt=""> <i
                        class="icon-chevron-down"></i>
                </div>
                <div class="header-dropdown__desc">
                    <ul>
                        <li class="language hidden-md hidden-lg"><a href="#" title="РУС" class="active">РУС</a><a href="#" title="ENG">ENG</a></li>
                        <li><a href="#" title="Выйти">Выйти</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <main class="main">


<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="#" title="Архив">Архив</a></li>
        <li><span>Moore-Barton</span></li>
    </ul>

    <form class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основные реквизиты</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основные реквизиты</a></li>
                        <li><a href="javascript:;" title="Служебная информация">Служебная информация</a></li>
                        <li><a href="javascript:;" title="История изменения">История изменения</a></li>
                        <li><a href="javascript:;" title="Служебные">Служебные</a></li>
                        <li><a href="javascript:;" title="Дела">Дела</a></li>
                        <li><a href="javascript:;" title="История изменения фонда">История изменения фонда</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title">Номер фонда</label>
                        <input type="text" name="fundId" value="443812" placeholder="" class="input-regular">
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Наименование фонда</label>
                        <input type="text" name="fundName" value="Moore-Barton" placeholder="" class="input-regular">
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Организация</label>
                        <select name="organization" class="input-regular chosen" data-placeholder=" ">
                            <option value="" hidden></option>
                            <option value="Туркестанский областной государственный архив" selected>Туркестанский
                                областной государственный архив
                            </option>
                            <option value="value2">value 2</option>
                            <option value="value3">value 3</option>
                            <option value="value4">value 4</option>
                            <option value="value5">value 5</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата первого поступления*</label>
                                <label class="date">
                                    <input type="text" name="firstIntroductionDate" value="15.01.2020" placeholder=""
                                           class="input-regular custom-datepicker" required>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата закрытия фонда</label>
                                <label class="date">
                                    <input type="text" name="closureDate" placeholder=""
                                           class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="input-group">
                        <label class="checkbox">
                            <input type="checkbox" name="valuable">
                            <span>Содержит в себе ценные документы</span>
                        </label>
                    </div>
                    <div class="input-group">
                        <label class="checkbox">
                            <input type="checkbox" name="published" checked>
                            <span>Опубликовано</span>
                        </label>
                    </div>
                    <br/>
                    <div class="input-group">
                        <label class="input-group__title">Дата выбытия из фонда</label>
                        <label class="date">
                            <input type="text" name="disposal" value="10.09.2020" placeholder=""
                                   class="input-regular custom-datepicker">
                        </label>
                    </div>
                    <div class="input-group has-error">
                        <label class="input-group__title">Причина выбытия из фонда</label>
                        <select name="organization" class="input-regular chosen" data-placeholder="">
                            <option value="" selected>-</option>
                            <option value="value1">value 1</option>
                            <option value="value2">value 2</option>
                            <option value="value3">value 3</option>
                            <option value="value4">value 4</option>
                            <option value="value5">value 5</option>
                        </select>
                        <span class="help-block"><strong>Текст ошибки</strong></span>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Примечание к выбытию из фонда</label>
                        <textarea name="disposalNote" placeholder="Заполните поле: Примечание к выбытию из фонда"
                                  class="input-regular"></textarea>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Примечание</label>
                        <textarea name="note" placeholder="Заполните поле: Примечание" class="input-regular"></textarea>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Дополнения</label>
                        <textarea name="additions" placeholder="Заполните поле: Дополнения"
                                  class="input-regular"></textarea>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Инструкция по работе с фондом</label>
                        <input type="hidden" name="instruction_document"
                               value="">
                        <div id="file1" class="file-upload">
                            <div id="file1_uploader" class="file">
                                <div class="progress">
                                    <div class="progress-bar"></div>
                                </div>
                                <span class="file__name">
                                    .docx, .doc, .xls, .xlsx, .pdf, .txt, .ppt, .pptx • 25 MB<br/>
                                    <strong>Загрузить документ</strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="input-group__title">Инструкция по работе с фондом2</label>
                        <input type="hidden" name="instruction_document2"
                               value="">
                        <div id="file2" class="file-upload">
                            <div id="file2_uploader" class="file">
                                <div class="progress">
                                    <div class="progress-bar"></div>
                                </div>
                                <span class="file__name">
                                    .docx, .doc, .xls, .xlsx, .pdf, .txt, .ppt, .pptx • 25 MB<br/>
                                    <strong>Загрузить документ</strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div>2</div>
                <div>3</div>
                <div>4</div>
                <div>
                    <table class="table records">
                        <colgroup>
                            <col span="1" style="width: 3%;">
                            <col span="1" style="width: 9%;">
                            <col span="1" style="width: 9%;">
                            <col span="1" style="width: 10%;">
                            <col span="1" style="width: 10%;">
                            <col span="1" style="width: 16%;">
                            <col span="1" style="width: 8%;">
                            <col span="1" style="width: 11%;">
                            <col span="1" style="width: 11%;">
                            <col span="1" style="width: 13%;">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Дело</th>
                            <th>Индекс дела</th>
                            <th>Год создания</th>
                            <th>Фонд</th>
                            <th>Организация</th>
                            <th>Штрих код</th>
                            <th>Дата создания</th>
                            <th>Дата изменения</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>hic</td>
                            <td>542161</td>
                            <td>1986</td>
                            <td>Moore-Barton</td>
                            <td>Туркестанский городской государственный архив</td>
                            <td>486791</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>2020-01-15 12:40:16</td>
                            <td>
                                <div class="action-buttons">
                                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="text-right">
                        <ul class="pagination">
                            <li class="previous_page "><span><i class="icon-chevron-left"></i></span></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li class="dots "><span>...</span></li>
                            <li><a href="#">498</a></li>
                            <li><a href="#">499</a></li>
                            <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="plain-text">
                        <strong>Администратор Panama DC</strong> : 2020-01-22 13:19:19<br/>
                        Было изменено поле <strong>"Номер фонда"</strong> c <span style="color: #FF7474">"443813"</span>
                        на <span style="color: #08D26F">"443812"</span><br/>
                        Было изменено поле <strong>"Дата первого поступления"</strong> c <span style="color: #FF7474">"2020-01-15 12:40:16"</span>
                        на <span style="color: #08D26F">"2020-01-15 00:00:00"</span>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
            <div>
                <button class="btn btn--red">Удалить</button>
            </div>
        </div>
    </form>
</div>

</main>

<footer class="footer">
</footer>
</div></div>
<script src="/admin/libs/jquery/dist/jquery.js"></script>
<script src="/admin/libs/maskedinput/maskedinput.js"></script>
<script src="/admin/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/admin/libs/chosen/chosen.jquery.js"></script>
<script src="/admin/libs/air-datepicker/dist/js/datepicker.js"></script>
<script type="text/javascript" src="/admin/libs/plupload/js/plupload.full.min.js"></script>
<script src="/admin/js/scripts.js"></script>


<!--Only this page's scripts-->
<script>
  var uploaders = new Array();

  initUploaders = function (uploaders) {
    console.log("initUploaders()");
    $(".file-upload").each(function () {
      var el = $(this),
        button = el.attr("id") + "_uploader",
        progressBar = el.find('.progress-bar'),
        input = el.siblings('input');
      console.log("Init uploader id:" + el.attr("id"));
      var uploader = new plupload.Uploader({
        runtimes: 'gears,html5,flash,silverlight,browserplus',
        browse_button: button,
        drop_element: button,
        max_file_size: '25mb',
        url: 'https://flowdoc.panama.kz/ajaxUploadFile',
        flash_swf_url: '/admin/libs/plupload/js/Moxie.swf',
        silverlight_xap_url: '/admin/libs/plupload/js/Moxie.xap',
        filters: [
          {title: "Document files", extensions: "docx,doc,xls,xlsx,pdf,txt,ppt,pptx"}
        ],
        unique_names: true,
        multiple_queues: false,
        multi_selection: false
      });

      uploader.bind('FilesAdded', function (up, files) {
        progressBar.css({width: 0});
        el.removeClass('error').removeClass('success').addClass('disabled');
        uploader.start();
      });

      uploader.bind("UploadProgress", function (up, file) {
        progressBar.css({width: file.percent + "%"});
      });

      uploader.bind("FileUploaded", function (up, file, response) {
        var obj = $.parseJSON(response.response.replace(/^.*?({.*}).*?$/gi, "$1"));
        input.val(obj.location);
        el.removeClass('disabled').removeClass('error').addClass('success');
        up.refresh();
      });

      uploader.bind("Error", function (up, err) {
        progressBar.css({width: 0});
        el.removeClass('disabled').removeClass('success').addClass('error');
        up.refresh();
      });

      uploader.init();

      uploaders.push(uploader);
    });
  };

  initUploaders(uploaders);
</script>
<!---->

<div id="message" class="modal" style="display: none;">
    <h4 class="title-secondary">Удаление</h4>
    <div class="plain-text">
        При удалении все данные будут удалены
    </div>
    <hr>
    <div class="buttons justify-end">
        <div><button class="btn btn--red">Удалить</button></div>
        <div><button class="btn" data-fancybox-close>Отмена</button></div>
    </div>
</div>

<!--
<script>
    $.fancybox.open({
      src: "#message",
      touch: false
    })
</script>-->


</body>
</html>
