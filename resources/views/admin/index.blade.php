
@include('admin.layouts.header');

    <main class="main">

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-4">
                <h1 class="title-primary" style="margin-bottom: 0">Архив</h1>
            </div>
            <div class="col-md-8 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        <a href="#" title="Расширенный поиск" class="btn"><i class="icon-search"></i> <span>Расширенный поиск</span></a>
                    </div>
                    <div>
                        <form class="input-button">
                            <input type="text" name="search" placeholder="Наименование фонда/Номер фонда" class="input-regular input-regular--solid" style="width: 282px;">
                            <button class="btn btn--green">Найти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block">
    <h2 class="title-secondary"></h2>

    <div class="text-right">
        <ul class="pagination">
            <li class="previous_page disabled"><span><i class="icon-chevron-left"></i></span></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li class="dots disabled"><span>...</span></li>
            <li><a href="#">498</a></li>
            <li><a href="#">499</a></li>
            <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
        </ul>
    </div>
</div>

    <div class="block collapsed">
        <div class="block__header">
            <h2 class="title-secondary">Отчет</h2>
            <i class="icon-chevron-up btn-collapse"></i>
        </div>

        <div class="block__body" style="display:none;">
            <table class="table report">
                <thead class="gray">
                <tr>
                    <th colspan="2"><i>Фондов зарегистрировано:</i> <strong>50</strong></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><strong>на организацию "Шымкентский городской государственный архив"</strong></td>
                    <td>
                        <div class="line-chart" data-value="3"><span></span></div>
                    </td>
                </tr>
                <tr>
                    <td><strong>на организацию "Государственное учреждение "Управление цифровизации, оказания государственных услуг и архивов Туркестанской области""</strong></td>
                    <td>
                        <div class="line-chart" data-value="2"><span></span></div>
                    </td>
                </tr>
                <tr>
                    <td><strong>на организацию "Сарыагаш"</strong></td>
                    <td>
                        <div class="line-chart" data-value="3"><span></span></div>
                    </td>
                </tr>
                <tr>
                    <td><strong>на организацию "Туркестанский областной государственный архив"</strong></td>
                    <td>
                        <div class="line-chart" data-value="1"><span></span></div>
                    </td>
                </tr>
                <tr>
                    <td><strong>на организацию "Жетысайский региональный государственный архив"</strong></td>
                    <td>
                        <div class="line-chart" data-value="4"><span></span></div>
                    </td>
                </tr>
                </tbody>
                <tfoot class="gray">
                <tr>
                    <td colspan="2">
                        <i>Фондов отредактировано:</i> <strong>1</strong><br/>
                        <i>Фондов удалено:</i> <strong>0</strong><br/>
                        <i>Особо ценных фондов:</i> <strong>0</strong>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

</main>

@include('admin.layouts.footer');
