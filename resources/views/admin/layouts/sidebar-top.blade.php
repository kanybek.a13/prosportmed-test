@yield('navbar')

<aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/" title="Главная" class="logo"><img src="/admin/img/logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">

            @can('edit_content')

            <li class="dropdown active">
                <a href="javascript:;" title="Наша команда"><i class="icon-archive"></i> Наша команда</a>
                <ul>
                    <li><a href="/admin/doctors" title="Список докторов">Список докторов</a></li>
                    <li><a href="/admin/new/doctor" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Оборудование"><i class="icon-reports"></i> Оборудование</a>
                <ul>
                    <li><a href="/admin/equipments" title="Список оборудований">Список оборудований</a></li>
                    <li><a href="/admin/new/equipment" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>

            @endcan
            @can('view_request')

            <li class="dropdown">
                <a href="javascript:;" title="Заявки"><i class="icon-directory"></i> Заявки</a>
                <ul>
                    <li><a href="/admin/requests" title="Список заявок">Список заявок</a></li>
                </ul>
            </li>

            @endcan
            @can('edit_content')

            <li class="dropdown">
                <a href="javascript:;" title="Отзывы"><i class="icon-organizations"></i> Отзывы</a>
                <ul>
                    <li><a href="/admin/reviews" title="Список заявок">Список отзывов</a></li>
                </ul>
            </li>

            @endcan
            @can('admin')

                <li class="dropdown">
                    <a href="javascript:;" title="Пользователи"><i class="icon-archive"></i> Пользователи</a>
                    <ul>
                        <li><a href="/admin/users" title="Список докторов">Список пользователей</a></li>
                        <li><a href="/admin/new/user" title="Добавить" class="add">+Добавить</a></li>
                    </ul>
                </li>

            @endcan
{{--            <li class="dropdown">--}}
{{--                <a href="javascript:;" title="Пользователи"><i class="icon-users"></i> Пользователи</a>--}}
{{--                <ul>--}}
{{--                    <li><a href="#" title="Ссылка">Ссылка</a></li>--}}
{{--                    <li><a href="#" title="Ссылка">Ссылка</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
        </ul>
    </div>
</aside>

