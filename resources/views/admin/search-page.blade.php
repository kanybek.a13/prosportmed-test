<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Flowdoc</title>

    <link rel="stylesheet" href="/admin/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/chosen/chosen.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/air-datepicker/dist/css/datepicker.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/css/style.css" type="text/css" media="screen"/>

    <link rel="apple-touch-icon" sizes="180x180" href="/admin/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/admin/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/admin/favicon/favicon-16x16.png">
    <link rel="manifest" href="/admin/favicon/site.webmanifest">
    <link rel="mask-icon" href="/admin/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <meta name="title" content="Заголовок">
    <meta name="description" content="Описание">
    <meta name="keywords" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Заголовок">
    <meta property="og:description" content="Описание">
    <meta property="og:url" content="/">
    <meta property="og:image" content="/admin/img/og-image.jpg">
    <meta property="og:site_name" content="localhost:9876">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Заголовок">
    <meta property="twitter:description" content="Описание">
    <meta property="twitter:image" content="/admin/img/og-image.jpg">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <meta property="twitter:url" content="/">
    <meta property="author" content="">
    <meta name="relap-image" content="/admin/img/og-image.jpg">
    <meta name="relap-title" content="Заголовок">
    <meta name="relap-description" content=" ">

</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
<aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/" title="Главная" class="logo"><img src="/admin/img/logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            <li class="dropdown active">
                <a href="javascript:;" title="Архив"><i class="icon-archive"></i> Архив</a>
                <ul>
                    <li><a href="#" title="Список фондов">Список фондов</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                    <li><a href="#" title="Список дел">Список дел</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Отчеты"><i class="icon-reports"></i> Отчеты</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Справочник"><i class="icon-directory"></i> Справочник</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Организации"><i class="icon-organizations"></i> Организации</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Пользователи"><i class="icon-users"></i> Пользователи</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
<div class="right-wrapper">
    <header class="header">
        <div class="container container-fluid">
            <a href="javascript:;" title="Свернуть/развернуть навигацию" class="menu-btn icon-menu"></a>
            <a href="/" title="Главная" class="logo hidden-md hidden-lg"><img src="/admin/img/logo-blue.svg" alt=""></a>
            <div class="language hidden-sm hidden-xs">
                <a href="#" title="РУС" class="active">РУС</a>
                <a href="#" title="ENG">ENG</a>
            </div>
            <div class="header-dropdown account-nav">
                <div class="header-dropdown__title">
                    <span>Добро пожаловать, Илья!</span> <img src="/admin/img/user.svg" alt=""> <i
                        class="icon-chevron-down"></i>
                </div>
                <div class="header-dropdown__desc">
                    <ul>
                        <li class="language hidden-md hidden-lg"><a href="#" title="РУС" class="active">РУС</a><a href="#" title="ENG">ENG</a></li>
                        <li><a href="#" title="Выйти">Выйти</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <main class="main">


<div class="container container-fluid">
    <form class="block">
        <div class="input-group">
            <label class="input-group__title">Поисковая фраза</label>
            <input type="text" name="text" placeholder="" class="input-regular">
        </div>
        <div class="collapse-block collapsed" style="display: none;" id="collapse1">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <label class="input-group__title">Организация</label>
                        <select name="organization" class="input-regular chosen" data-placeholder=" ">
                            <option value="" selected hidden> </option>
                            <option value="value1">value 1</option>
                            <option value="value2">value 2</option>
                            <option value="value3">value 3</option>
                            <option value="value4">value 4</option>
                            <option value="value5">value 5</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Год создания: с</label>
                                <label class="date">
                                    <input type="text" name="creationDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="creationDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата сдачи в архив: с</label>
                                <label class="date">
                                    <input type="text" name="archiveDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="archiveDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата уничтожения: с</label>
                                <label class="date">
                                    <input type="text" name="destroyDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="destroyDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">Дата выдачи: с</label>
                                <label class="date">
                                    <input type="text" name="releaseDateFrom" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label class="input-group__title">до</label>
                                <label class="date">
                                    <input type="text" name="releaseDateTo" placeholder="" class="input-regular custom-datepicker">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <label class="input-group__title">&nbsp;</label>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="archived">
                                <span>В архиве</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="released">
                                <span>Выдано</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="storage">
                                <span>Оперативное хранение</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="destroyed">
                                <span>Уничтожено</span>
                            </label>
                        </div>
                        <div class="input-group">
                            <label class="checkbox">
                                <input type="checkbox" name="formed">
                                <span>Дело сформировано</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-group">
            <a href="javascript:;" title="Расширенный фильтр" class="grey-link small collapse-btn"
               data-target="collapse1">Расширенный фильтр</a></div>
        <div class="buttons">
            <div><button class="btn btn--green">Искать</button></div>
            <div><button class="btn btn--yellow">Сбросить</button></div>
        </div>
    </form>

    <div class="block">
    <h2 class="title-secondary">Список записей</h2>
    <table class="table records">
        <colgroup>
            <col span="1" style="width: 3%;">
            <col span="1" style="width: 20%;">
            <col span="1" style="width: 12%;">
            <col span="1" style="width: 20%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 15%;">
            <col span="1" style="width: 15%;">
        </colgroup>
        <thead>
        <tr>
            <th>#</th>
            <th>Наименование фонда</th>
            <th>Номер фонда</th>
            <th>Организация</th>
            <th>Дата создания</th>
            <th>Дата изменения</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>67</td>
            <td>Langosh PLC	</td>
            <td>958000</td>
            <td>Государственное учреждение "Управление цифровизации, оказания государственных услуг и архивов Туркестанской области"</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19 Администратор Panama DC</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        <tr>
            <td>68</td>
            <td>Moore-Barton</td>
            <td>443812</td>
            <td>Туркестанский областной государственный архив	</td>
            <td>2020-01-15 12:40:16	</td>
            <td>2020-01-22 13:19:19</td>
            <td>
                <div class="action-buttons">
                    <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                    <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                    <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="text-right">
        <ul class="pagination">
            <li class="previous_page disabled"><span><i class="icon-chevron-left"></i></span></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li class="dots disabled"><span>...</span></li>
            <li><a href="#">498</a></li>
            <li><a href="#">499</a></li>
            <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
        </ul>
    </div>
</div>
</div>

</main>

<footer class="footer">
</footer>
</div></div>
<script src="/admin/libs/jquery/dist/jquery.js"></script>
<script src="/admin/libs/maskedinput/maskedinput.js"></script>
<script src="/admin/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/admin/libs/chosen/chosen.jquery.js"></script>
<script src="/admin/libs/air-datepicker/dist/js/datepicker.js"></script>
<script type="text/javascript" src="/admin/libs/plupload/js/plupload.full.min.js"></script>
<script src="/admin/js/scripts.js"></script>


<!--Only this page's scripts-->

<!---->

<div id="message" class="modal" style="display: none;">
    <h4 class="title-secondary">Удаление</h4>
    <div class="plain-text">
        При удалении все данные будут удалены
    </div>
    <hr>
    <div class="buttons justify-end">
        <div><button class="btn btn--red">Удалить</button></div>
        <div><button class="btn" data-fancybox-close>Отмена</button></div>
    </div>
</div>

<!--
<script>
    $.fancybox.open({
      src: "#message",
      touch: false
    })
</script>-->


</body>
</html>
