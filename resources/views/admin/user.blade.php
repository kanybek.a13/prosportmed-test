<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Flowdoc</title>

    <link rel="stylesheet" href="/admin/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/chosen/chosen.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/libs/air-datepicker/dist/css/datepicker.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/admin/css/style.css" type="text/css" media="screen"/>

    <link rel="apple-touch-icon" sizes="180x180" href="/admin/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/admin/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/admin/favicon/favicon-16x16.png">
    <link rel="manifest" href="/admin/favicon/site.webmanifest">
    <link rel="mask-icon" href="/admin/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <meta name="title" content="Заголовок">
    <meta name="description" content="Описание">
    <meta name="keywords" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Заголовок">
    <meta property="og:description" content="Описание">
    <meta property="og:url" content="/">
    <meta property="og:image" content="/admin/img/og-image.jpg">
    <meta property="og:site_name" content="localhost:9876">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Заголовок">
    <meta property="twitter:description" content="Описание">
    <meta property="twitter:image" content="/admin/img/og-image.jpg">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <meta property="twitter:url" content="/">
    <meta property="author" content="">
    <meta name="relap-image" content="/admin/img/og-image.jpg">
    <meta name="relap-title" content="Заголовок">
    <meta name="relap-description" content=" ">

</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
<aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/" title="Главная" class="logo"><img src="/admin/img/logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            <li class="dropdown active">
                <a href="javascript:;" title="Архив"><i class="icon-archive"></i> Архив</a>
                <ul>
                    <li><a href="#" title="Список фондов">Список фондов</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                    <li><a href="#" title="Список дел">Список дел</a></li>
                    <li><a href="#" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Отчеты"><i class="icon-reports"></i> Отчеты</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Справочник"><i class="icon-directory"></i> Справочник</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Организации"><i class="icon-organizations"></i> Организации</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Пользователи"><i class="icon-users"></i> Пользователи</a>
                <ul>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                    <li><a href="#" title="Ссылка">Ссылка</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
<div class="right-wrapper">
    <header class="header">
        <div class="container container-fluid">
            <a href="javascript:;" title="Свернуть/развернуть навигацию" class="menu-btn icon-menu"></a>
            <a href="/" title="Главная" class="logo hidden-md hidden-lg"><img src="/admin/img/logo-blue.svg" alt=""></a>
            <div class="language hidden-sm hidden-xs">
                <a href="#" title="РУС" class="active">РУС</a>
                <a href="#" title="ENG">ENG</a>
            </div>
            <div class="header-dropdown account-nav">
                <div class="header-dropdown__title">
                    <span>Добро пожаловать, Илья!</span> <img src="/admin/img/user.svg" alt=""> <i
                        class="icon-chevron-down"></i>
                </div>
                <div class="header-dropdown__desc">
                    <ul>
                        <li class="language hidden-md hidden-lg"><a href="#" title="РУС" class="active">РУС</a><a href="#" title="ENG">ENG</a></li>
                        <li><a href="#" title="Выйти">Выйти</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <main class="main">


<div class="container container-fluid">
    <div class="block">
        <h2 class="title-primary">Пользователь</h2>
        <div class="input-group ">
            <label for="organizations"
                   class="input-group__title">Организация <span
                    class="required">*</span></label>
            <select name="organizations[]" id="organizations" class="input-regular chosen"
                    data-placeholder=" ">
                <option value="" data-permissions="">-</option>
                <option value="29"
                        data-permissions="5,6,7">
                    Test
                </option>
                <option value="15"
                        data-permissions="1,4,6,7">
                    Арысский городской государственный архив
                </option>
                <option value="9"
                        data-permissions="5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36">
                    Государственное учреждение &quot;Управление цифровизации, оказания государственных услуг и архивов Туркестанской области&quot;
                </option>
                <option value="14"
                        data-permissions="2,3">
                    Государственный архив общественно-политической истории Туркестанской области
                </option>
                <option value="16"
                        data-permissions="6,7">
                    Государственный архив района Байдибек
                </option>
                <option value="12"
                        data-permissions="11,12">
                    Жетысайский региональный государственный архив
                </option>
                <option value="17"
                        data-permissions="15,16">
                    Казыгуртский районный государственный архив
                </option>
                <option value="18"
                        data-permissions="30,8">
                    Келесский районный государственный архив
                </option>
                <option value="13"
                        data-permissions="17,12">
                    Кентауский региональный государственный архив
                </option>
                <option value="19"
                        data-permissions="1,8">
                    Мактааральский районный государственный архив
                </option>
                <option value="20"
                        data-permissions="9,13">
                    Ордабасинский районный государственный архив
                </option>
                <option value="21"
                        data-permissions="1,9,8">
                    Отрарский районный государственный архив
                </option>
                <option value="22"
                        data-permissions="36,14,11">
                    Сайрамский районный государственный архив
                </option>
                <option value="10"
                        data-permissions="5,6,7,8">
                    Сарыагаш
                </option>
                <option value="23"
                        data-permissions="2,5,9">
                    Сарыагашский районный государственный архив
                </option>
                <option value="24"
                        data-permissions="4,8,5">
                    Сузакский районный государственный архив
                </option>
                <option value="25"
                        data-permissions="3,4,6">
                    Толебийский районный государственный архив
                </option>
                <option value="26"
                        data-permissions="7,8,9">
                    Туркестанский городской государственный архив
                </option>
                <option value="11"
                        data-permissions="9,10,11">
                    Туркестанский областной государственный архив
                </option>
                <option value="27"
                        data-permissions="11,12">
                    Тюлькубасский районный государственный архив
                </option>
                <option value="28"
                        data-permissions="5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31">
                    Шардаринский районный государственный архив
                </option>
                <option value="8"
                        data-permissions="13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36">
                    Шымкентский городской государственный архив
                </option>
            </select>
        </div>
    </div>

    <div class="block">
        <h2 class="title-secondary">Доступы пользователя</h2>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="5" type="checkbox">
                <span>Просмотр списка структурных подразделений</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="6" type="checkbox">
                <span>Создание записи структурного подразделения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="7" type="checkbox">
                <span>Редактирование записи структурного подразделения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="8" type="checkbox">
                <span>Удаление записи структурного подразделения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="9" type="checkbox">
                <span>Просмотр списка пользователей</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="10" type="checkbox">
                <span>Создание записи пользователя</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="11" type="checkbox">
                <span>Редактирование записи пользователя</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="12" type="checkbox">
                <span>Удаление записи пользователя</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="13" type="checkbox">
                <span>Просмотр списка фондов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="14" type="checkbox">
                <span>Создание записи фонда</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="15" type="checkbox">
                <span>Редактирование записи фонда</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="16" type="checkbox">
                <span>Удаление записи фонда</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="17" type="checkbox">
                <span>Просмотр списка дел</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="18" type="checkbox">
                <span>Создание записи дела</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="19" type="checkbox">
                <span>Редактирование записи дела</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="20" type="checkbox">
                <span>Удаление записи дела</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="21" type="checkbox">
                <span>Создание документа/решения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="22" type="checkbox">
                <span>Редактирование документа/решения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="23" type="checkbox">
                <span>Удаление документа/решения</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="24" type="checkbox">
                <span>Загрузка скан-образов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="25" type="checkbox">
                <span>Поиск</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="26" type="checkbox">
                <span>Работа с секретными документами</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="27" type="checkbox">
                <span>Работа с публичными документами</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="28" type="checkbox">
                <span>Просмотр статистики фондов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="29" type="checkbox">
                <span>Просмотр статистики организаций</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="30" type="checkbox">
                <span>Просмотр статистики пользователей</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="31" type="checkbox">
                <span>Просмотр списка записей справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="32" type="checkbox">
                <span>Создание записи справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="33" type="checkbox">
                <span>Редактирование записи справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="34" type="checkbox">
                <span>Удаление записи справочника</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="35" type="checkbox">
                <span>Просмотр статистики справочников</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="36" type="checkbox">
                <span>Просмотр статистики дел</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="37" type="checkbox">
                <span>Просмотр списка отчетов</span>
            </label>
        </div>
        <div class="input-group">
            <label class="checkbox">
                <input name="permissions[]" value="38" type="checkbox">
                <span>Просмотр отчета по статистике</span>
            </label>
        </div>
    </div>
</div>

</main>

<footer class="footer">
</footer>
</div></div>
<script src="/admin/libs/jquery/dist/jquery.js"></script>
<script src="/admin/libs/maskedinput/maskedinput.js"></script>
<script src="/admin/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/admin/libs/chosen/chosen.jquery.js"></script>
<script src="/admin/libs/air-datepicker/dist/js/datepicker.js"></script>
<script type="text/javascript" src="/admin/libs/plupload/js/plupload.full.min.js"></script>
<script src="/admin/js/scripts.js"></script>


<!--Only this page's scripts-->
<script>
  $('#organizations').change(function () {
    var option = $("option:selected", "#organizations"),
      permissions = $.map(option.attr("data-permissions").split(','), function(value){
        return +value;
      });

    $("[name='permissions[]']").each(function () {
      var input = $(this);
      if (permissions.includes(parseInt(input.val()))) {
        input.prop('checked', true);
      } else {
        input.prop('checked', false)
      }
    });
  })
</script>
<!---->

<div id="message" class="modal" style="display: none;">
    <h4 class="title-secondary">Удаление</h4>
    <div class="plain-text">
        При удалении все данные будут удалены
    </div>
    <hr>
    <div class="buttons justify-end">
        <div><button class="btn btn--red">Удалить</button></div>
        <div><button class="btn" data-fancybox-close>Отмена</button></div>
    </div>
</div>

<!--
<script>
    $.fancybox.open({
      src: "#message",
      touch: false
    })
</script>-->


</body>
</html>
