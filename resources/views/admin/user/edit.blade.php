@include('admin.layouts.header')
@can('admin')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><a href="#" title="Доктор">Пользователь</a></li>
        <li><span>{{$user->name}}</span></li>
    </ul>

    <form class="block" method="post" action="/admin/user">
        @csrf
        @method('put')

        <input type="hidden" name="id" value="{{$user->id}}">

        <div class="tabs-contents">
            <div class="active">
                <div class="input-group">
                    <label class="input-group__title"> ФИО</label>
                    <input type="text" name="name" value="{{$user->name}}" placeholder="ФИО" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Почта</label>
                    <input type="email" name="email" value="{{$user->email}}" placeholder="Почта" class="input-regular">
                </div>
                <br>
                <div class="input-group">
                    <label class="input-group__title"> Role</label>
                    <select name="role" class="input-regular chosen" data-placeholder="Role">
                        @foreach($roles as $role)
                            <option value="{{$role->name}}"
                                @if($role->name === $user_role)
                                   selected
                                @endif>{{$role->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <br>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
            <div>
                <button class="btn btn--red"><a href="/admin/user/delete/{{$user->id}}" title="Удалить"></a>Удалить</button>
            </div>
        </div>
    </form>
</div>

</main>
@endcan
@include('admin.layouts.footer');
