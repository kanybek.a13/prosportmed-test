@include('admin.layouts.header')
@can('admin')

    <div class="container container-fluid">
        <br>
        <ul class="breadcrumbs">
            <li><a href="#" title="Доктор">Пользователь</a></li>
            <li><span>Новый</span></li>
        </ul>

        <form class="block" method="post" action="/admin/user">
            @csrf

            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="name" value="" placeholder="ФИО" class="input-regular">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Почта</label>
                        <input type="email" name="email" value="" placeholder="Почта" class="input-regular">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Role</label>
                        <select name="role" class="input-regular chosen" data-placeholder="Role">
                            @foreach($roles as $role)
                                <option value="{{$role->name}}">
                                       {{$role->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                </div>
            </div>
            <hr>
            <div class="buttons">
                <div>
                    <button type="submit" class="btn btn--green">Сохранить</button>
                </div>
            </div>
        </form>
    </div>

    </main>
@endcan
@include('admin.layouts.footer');
