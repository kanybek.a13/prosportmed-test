@include('admin.layouts.header')
@can('admin')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="#" title="Доктор">Доктор</a></li>
        <li><span>{{$user->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$user->id}}</div>
            <h1 class="fund-header__title">{{$user->name}}</h1>
{{--            <div class="fund-header__organization">Туркестанский областной государственный архив</div>--}}
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$user->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения	</div>
                <div class="property__text">{{$user->updated_at}}<br>
                    Администратор Panama DC</div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">

            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                </div>
            </div>

            <div class="tabs-contents">
                <div class="active">

                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="/admin/user/edit/{{$user->id}}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="name" value="{{$user->name}}" placeholder="ФИО" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Почта</label>
                        <input type="email" name="email" value="{{$user->email}}" placeholder="Почта" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Role</label>
                        <input type="text" name="role" value="{{$user_role->name}}" placeholder="Role" class="input-regular" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </main>
@endcan
@include('admin.layouts.footer');
