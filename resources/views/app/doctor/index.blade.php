
@include('app.layouts.header');


<!--Doctors-->
<section class="grey">
    <div class="container">
        <h3 class="title-primary">Наши доктора</h3>
            <div class="row row--multiline">

                @foreach($doctors as $doctor)
                    <div class="col-md-4 col-sm-6">
                        <a href="/doctor/{{$doctor->id}}" title="" class="doctor">
                            <div class="doctor__image">
                                <img src="https://image.freepik.com/free-vector/doctor-character-background_1270-84.jpg" alt="">
{{--                                <img src="/app/img/doctors/asylbaeva.jpg" alt="">--}}
                            </div>
                            <div class="doctor__desc">
                                <h4 class="doctor__name">{{$doctor->name}}</h4>
                                <div class="doctor__position">{{$doctor->position}}</div>
                            </div>
                        </a>
                    </div>
                @endforeach
        </div>
    </div>

</section>

@extends('app.layouts.footer')

@section('content')
<!--Only this page's scripts-->

<!---->
@endsection
