@extends('app.layouts.header')

<section>
    <div class="container container--md">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><a href="/doctors" title="Наши доктора">Наши доктора</a></li>
            <li><span>{{$doctor->name}}</span></li>
        </ul>

        <article class="article">
            <h1 class="title-primary">{{$doctor->name}}</h1>
            <div class="row row--multiline">
                <div class="col-sm-4">
{{--                    <img class="article-image" src="/app/img/doctors/kim.png" alt="">--}}
                    <img class="article-image" src="https://image.freepik.com/free-vector/doctor-character-background_1270-84.jpg" alt="">
                </div>
                <div class="col-sm-8">
                    <div class="text">
                        <p>{{$doctor->name}} - {{$doctor->title}}.</p>
                        <p>{{$doctor->body}}.</p>
                    </div>
                </div>
            </div>
        </article>

        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
    </div>
</section>

</main>

@extends('app.layouts.footer')

@section('content')
<!--Only this page's scripts-->
<script>
    $('.doctors').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.equipment').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.reviews').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: false,
        fade: true,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>'
    });

    $('.partners').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        swipe: false,
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
</script>
<!---->
@endsection
