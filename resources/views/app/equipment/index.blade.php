
@include('app.layouts.header');


<!--Equipments-->
<section class="grey">
    <div class="container">
        <h3 class="title-primary">Наши оборудовании</h3>
        <div class="row row--multiline">

            @foreach($equipments as $equipment)
                <div class="col-md-4 col-sm-6">
                    <a href="/equipment/{{$equipment->id}}" title="" class="equipment">
                        <div class="equipment__image">
{{--                            <img src="https://image.freepik.com/free-vector/equipment-character-background_1270-84.jpg" alt="">--}}
                            <img src="{{$equipment->image}}" alt="">
                        </div>
                        <div class="equipment__desc">
                            <h4 class="equipment__name">{{$equipment->name}}</h4>
{{--                            <div class="equipment__position">{{$equipment->}}</div>--}}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

</section>

@extends('app.layouts.footer')

@section('content')
    <!--Only this page's scripts-->

    <!---->
@endsection
