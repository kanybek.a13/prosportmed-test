@include('app.layouts.header');

<section>
    <div class="container container--sm">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><span>Оборудование</span></li>
        </ul>
        <article class="article">
            <h1 class="title-primary">Оборудование</h1>
            <p class="annotation">
                {{$equipment->name}}.
            </p>
            <div class="equipment-preview">
                <a href="/app/img/gallery/SSE_4499.jpg" title="" data-fancybox="galery"><img src="/app/img/gallery/SSE_4499.jpg" alt=""></a>
                <a href="/app/img/gallery/SSE_4502.jpg" title="" data-fancybox="galery"><img src="/app/img/gallery/SSE_4502.jpg" alt=""></a>
                <a href="/app/img/gallery/SSE_4505.jpg" title="" data-fancybox="galery"><img src="/app/img/gallery/SSE_4505.jpg" alt=""></a>
                <a href="/app/img/gallery/SSE_4510.jpg" title="" data-fancybox="galery"><img src="/app/img/gallery/SSE_4510.jpg" alt=""></a>
                <a href="/app/img/gallery/SSE_4514.jpg" title="" data-fancybox="galery"><img src="/app/img/gallery/SSE_4514.jpg" alt=""></a>
                <a href="/app/img/gallery/SSE_4517.jpg" title="" data-fancybox="galery"><img src="/app/img/gallery/SSE_4517.jpg" alt=""></a>
                <a href="/app/img/gallery/SSE_4520.jpg" title="" data-fancybox="galery"><img src="/app/img/gallery/SSE_4520.jpg" alt=""></a>
            </div>
            <div class="carousel-regular equipment">
                <a href="javascript:;" title=""><img src="{{$equipment->image}}" alt=""></a>
{{--                <a href="javascript:;" title=""><img src="/app/img/gallery/SSE_4502.jpg" alt=""></a>--}}
{{--                <a href="javascript:;" title=""><img src="/app/img/gallery/SSE_4505.jpg" alt=""></a>--}}
{{--                <a href="javascript:;" title=""><img src="/app/img/gallery/SSE_4510.jpg" alt=""></a>--}}
{{--                <a href="javascript:;" title=""><img src="/app/img/gallery/SSE_4514.jpg" alt=""></a>--}}
{{--                <a href="javascript:;" title=""><img src="/app/img/gallery/SSE_4517.jpg" alt=""></a>--}}
{{--                <a href="javascript:;" title=""><img src="/app/img/gallery/SSE_4520.jpg" alt=""></a>--}}
            </div>
            <div class="text">
                <p>{{$equipment->description}}.</p>
            </div>
        </article>
        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
    </div>
</section>
    @extends('app.layouts.footer')

    @section('content')

<!--Only this page's scripts-->
<script>
    $('.equipment').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        swipe: false,
        asNavFor: ".equipment-preview",
        swipeToSlide: true,
        prevArrow: '<i class="icon-left"></i>',
        focusOnSelect: true,
        nextArrow: '<i class="icon-right"></i>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
    $('.equipment-preview').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: false,
        asNavFor: ".equipment",
        fade: true,
        arrows: false
    });
</script>
<!---->

<div id="request" class="modal modal--auto" style="display:none;">
    <div class="text-center">
        <h3 class="title-primary">Заявка</h3>
        <hr>
        <div class="subtitle">
            Если у Вас возникли вопросы, отправьте заявку на обратный звонок и мы с Вами свяжемся в ближайшее время
        </div>
    </div>

    <form action="">
        <div class="input-group">
            <input name="name" type="text" class="input-regular" placeholder="Ваше имя" required>
        </div>
        <div class="input-group">
            <input name="phone" type="tel" onfocus="$(this).inputmask('+7 999 999 99 99')" class="input-regular" placeholder="Номер телефона" required>
        </div>
        <div class="input-group">
            <input name="email" type="email" class="input-regular" placeholder="Email" required>
        </div>
        <div class="text-center"><button type="submit" class="btn">Отправить</button></div>
    </form>
</div>

<div id="roadMap" style="display:none;">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d46465.04860766074!2d77.28510563416387!3d43.291949183630216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x22011103cacc06be!2z0JDQutCw0LTQtdC80LjRjyDQsdC-0LrRgdCwIEFJQkE!5e0!3m2!1sru!2skz!4v1564389355656!5m2!1sru!2skz" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

</body>
</html>
