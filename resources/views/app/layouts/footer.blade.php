
</main>

<footer class="footer">
    <div class="container">
        <div class="row row--multiline">
            <div class="col-md-2">
                <img class="footer__logo" src="/app/img/logo.svg" alt="">
            </div>
            <div class="col-md-5">
                <a href="#roadMap" title="" data-fancybox="" class="contact">
                    <i class="icon-pin"></i>
                    <span>Республика Казахстан, Алматинская область, Талгарский район, Алатауский сельский округ, село Рыскулово, Всемирная Академия бокса</span>
                </a>
                <a href="tel:+7-7273-31-51-52" title="" class="contact">
                    <i class="icon-phone"></i>
                    <span>+7-7273-31-51-52 (3130)</span>
                </a>
                <a href="tel:+7-702-403-88-36" title="" class="contact">
                    <i class="icon-whatsapp"></i>
                    <span>+7-702-403-88-36</span>
                </a>
                <div class="socials">
                    <a href="#" title="" target="_blank" class="icon-instagram"></a>
                    <a href="#" title="" target="_blank" class="icon-vk"></a>
                    <a href="#" title="" target="_blank" class="icon-twitter"></a>
                    <a href="#" title="" target="_blank" class="icon-facebook"></a>
                </div>
            </div>
            <div class="col-md-5">
                <nav class="sitemap">
                    <ul>
                        <li><a href="#" title="Услуги">Услуги</a></li>
                        <li><a href="#" title="Пакеты">Пакеты</a></li>
                        <li><a href="#" title="О центре">О центре</a></li>
                        <li><a href="#" title="Наши доктора">Наши доктора</a></li>
                        <li><a href="#" title="Контакты">Контакты</a></li>
                        <li><a href="#" title="Личный кабинет врача">Личный кабинет врача</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <hr>
        <div class="copyright">2019, Современный Центр спортивной медицины, реабилитации и подготовки «PROSPORT»</div>
    </div>
</footer>
</div>

<div class="scroll-up icon-up-material compensate-for-scrollbar"></div>
<script src="/app/libs/jquery/dist/jquery.js"></script>
<script src="/app/js/scripts.js"></script>
<script src="/app/js/parallax.min.js"></script>
<script src="/app/libs/maskedinput/maskedinput.js"></script>
<script src="/app/libs/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="/app/libs/slick-carousel/slick/slick.js"></script>s

<!--Only this page's scripts-->
    @yield('content')
<!---->

<div id="request" class="modal modal--auto" style="display:none;">
    <div class="text-center">
        <h3 class="title-primary">Заявка</h3>
        <hr>
        <div class="subtitle">
            Если у Вас возникли вопросы, отправьте заявку на обратный звонок и мы с Вами свяжемся в ближайшее время
        </div>
    </div>

    <form action="/contact" method="post">
        @csrf

        <div class="input-group">
            <input name="name" type="text" class="input-regular" placeholder="Ваше имя" required>
        </div>
        <div class="input-group">
            <input name="phone" type="tel" onfocus="$(this).inputmask('+7 999 999 99 99')" class="input-regular" placeholder="Номер телефона" required>
        </div>
        <div class="input-group">
            <input name="email" type="email" class="input-regular" placeholder="Email" required>
        </div>
        <div class="text-center"><button type="submit" class="btn">Отправить</button></div>
    </form>
</div>

<div id="roadMap" style="display:none;">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d46465.04860766074!2d77.28510563416387!3d43.291949183630216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x22011103cacc06be!2z0JDQutCw0LTQtdC80LjRjyDQsdC-0LrRgdCwIEFJQkE!5e0!3m2!1sru!2skz!4v1564389355656!5m2!1sru!2skz" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

</body>
</html>
