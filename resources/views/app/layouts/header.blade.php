<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Title</title>

    <link rel="stylesheet" href="/app/libs/fancybox/dist/jquery.fancybox.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/app/libs/slick-carousel/slick/slick.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/app/css/style.css" type="text/css" media="screen"/>

    <link rel="apple-touch-icon" sizes="57x57" href="/app/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/app/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/app/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/app/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/app/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/app/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/app/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/app/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/app/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/app/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/app/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/app/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/app/favicon/favicon-16x16.png">
    <link rel="manifest" href="/app/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/app/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="title" content="Заголовок">
    <meta name="description" content="Описание">
    <meta name="keywords" content="">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Заголовок">
    <meta property="og:description" content="Описание">
    <meta property="og:url" content="/">
    <meta property="og:image" content="/app/img/og-image.jpg">
    <meta property="og:site_name" content="localhost:9876">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Заголовок">
    <meta property="twitter:description" content="Описание">
    <meta property="twitter:image" content="/app/img/og-image.jpg">
    <meta property="twitter:site" content="">
    <meta property="twitter:creator" content="">
    <meta property="twitter:url" content="/">
    <meta property="author" content="">
    <meta name="relap-image" content="/app/img/og-image.jpg">
    <meta name="relap-title" content="Заголовок">
    <meta name="relap-description" content=" ">

</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    <header class="header">
        <div class="top">
            <div class="container">
                <a href="/" title="Главная" class="header__logo"><img src="/app/img/logo.svg" alt=""></a>
                <div class="header__contacts">
                    <a href="#roadMap" title="" data-fancybox="" class="contact">
                        <i class="icon-pin"></i>
                        <span>Схема проезда</span>
                    </a>
                    <div>
                        <a href="tel:+7-7273-31-51-52" title="" class="contact">
                            <i class="icon-phone"></i>
                            <span>+7-7273-31-51-52 (3130)</span>
                        </a>
                        <a href="tel:+7-702-403-88-36" title="" class="contact">
                            <i class="icon-whatsapp"></i>
                            <span>+7-702-403-88-36</span>
                        </a>
                    </div>
                </div>
                <div class="mobile-menu">
                    <a href="/admin/login" title="Личный кабинет" class="ghost-btn">Личный кабинет</a>
                    <nav class="menu hidden-md hidden-lg">
                        <ul>
                            <li><a href="#" title="Услуги">Услуги</a></li>
                            <li><a href="#" title="Пакеты">Пакеты</a></li>
                            <li class="dropdown">
                                <a href="javascript:;" title="О нас">О нас</a>
                                <ul>
                                    <li><a href="#" title="О центре">О центре</a></li>
                                    <li><a href="#" title="Наши доктора">Наши доктора</a></li>
                                </ul>
                            </li>
                            <li><a href="#" title="Контакты">Контакты</a></li>
                        </ul>
                    </nav>
                    <a href="#request" title="Оставить заявку" data-fancybox class="btn hidden-md hidden-lg">Оставить заявку</a>
                    <div class="socials">
                        <a href="#" title="" target="_blank" class="icon-instagram"></a>
                        <!--                        <a href="#" title="" target="_blank" class="icon-vk"></a>-->
                        <!--                        <a href="#" title="" target="_blank" class="icon-twitter"></a>-->
                        <a href="#" title="" target="_blank" class="icon-facebook"></a>
                    </div>
                    <div class="language">
                        <a href="#" title="Русский" class="active">РУС</a><a href="#" title="Қазақша">ҚАЗ</a>
                    </div>
                </div>
                <div class="menu-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="bottom compensate-for-scrollbar">
            <div class="container">
                <nav class="menu">
                    <ul>
                        <li><a href="/services" title="Услуги">Услуги</a></li>

                        <li><a href="/equipments" title="Оборудовани">Оборудовани</a></li>

                        <li class="dropdown">
                            <a href="javascript:;" title="О нас">О нас</a>
                            <ul>
                                <li><a href="/article" title="О центре">О центре</a></li>
                                <li><a href="/doctors" title="Наши доктора">Наши доктора</a></li>
                            </ul>
                        </li>

                        <li><a href="#" title="Контакты">Контакты</a>
                        </li>
                    </ul>
                </nav>
{{--                <form action="/request" method="post">Отправить</form>--}}
                <a href="#request" title="Оставить заявку" data-fancybox class="ghost-btn ghost-btn--white">Оставить заявку</a>
            </div>
        </div>
    </header>

    <main class="main">

