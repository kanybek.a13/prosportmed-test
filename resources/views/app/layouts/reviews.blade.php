<!--Reviews-->
<section class="reviews-section">
    <div class="container">
        <h3 class="title-primary text-center">Отзывы</h3>
        <div class="reviews carousel-regular">
            <div class="review">
                <img src="/app/img/review-avatar.png" alt="" class="review__avatar">
                <div class="review__text">
                    <a href="#" data-fancybox="" title="">“Хотел бы поблагодарить весь персонал медицинского центра «PROSPORT»  за проявленный профессионализм во время моего реабилитационного периода после Олимпийских игр”</a>
                </div>
                <div class="review__author">Данияр Елеусинов</div>
                <div class="review__author-position">Олимпийский чемпион по боксу</div>
            </div>
            <div class="review">
                <img src="/app/img/review-avatar.png" alt="" class="review__avatar">
                <div class="review__text">
                    <a href="#" data-fancybox="" title="">“Хотел бы поблагодарить весь персонал медицинского центра «PROSPORT»  за проявленный профессионализм во время моего реабилитационного периода после Олимпийских игр”</a>
                </div>
                <div class="review__author">Данияр Елеусинов</div>
                <div class="review__author-position">Олимпийский чемпион по боксу</div>
            </div>
            <div class="review">
                <img src="/app/img/review-avatar.png" alt="" class="review__avatar">
                <div class="review__text">
                    <a href="#" data-fancybox="" title="">“Хотел бы поблагодарить весь персонал медицинского центра «PROSPORT»  за проявленный профессионализм во время моего реабилитационного периода после Олимпийских игр”</a>
                </div>
                <div class="review__author">Данияр Елеусинов</div>
                <div class="review__author-position">Олимпийский чемпион по боксу</div>
            </div>
        </div>
    </div>
    <div class="reviews-section__bg">
        <img src="/app/img/reviews-bg.png" alt="">
        <img src="/app/img/review-bg.png" alt="">
        <img src="/app/img/reviews-bg.png" alt="">
    </div>
</section>
