@include('app.layouts.header');


<section id="services" class="grey padding-bottom-none">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><span>Услуги</span></li>
        </ul>
        <h1 class="title-primary">Услуги</h1>
        <div class="spoilers services">
            <div class="spoiler">
                <div class="spoiler__title"><span>Консультации</span></div>
                <div class="spoiler__desc">

                    @foreach($services as $service)
                        <div class="row row--multiline">
                            <div class="col-xs-8 col-md-10">
                                <a href="/service/{{$service->id}}" title="{{$service->name}}">{{$service->name}}</a></div>
                            <div class="col-xs-4 col-md-2 text-right">{{$service->price}}</div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>

<section id="packages" class="grey padding-bottom-none">
    <div class="container">
        <h1 class="title-primary">Пакеты</h1>
        <div class="spoilers services">
            <div class="spoiler">

                @foreach($packages as $package)
                    <div class="spoiler__title"><span>{{$package->name}}</span></div>
                    <div class="spoiler__desc">
                        <div class="row row--multiline">
                            <div class="col-xs-8 col-md-10">

                                @foreach($package->services as $service)
                                    <p><a href="/service/{{$service->id}}" title="{{$service->name}}">{{$service->name}}</a></p>
                                @endforeach

                            </div>
                            <div class="col-xs-4 col-md-2 text-right">38 000</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

<section id="lab" class="grey">
    <div class="container">
        <h1 class="title-primary">Платные лабораторные услуги</h1>
        <div class="spoilers services">
            <div class="spoiler">
                <div class="spoiler__title"><span>БИОХИМИЧЕСКИЕ ИССЛЕДОВАНИЯ КРОВИ</span></div>
                <div class="spoiler__desc">
                    <div class="row row--multiline hidden-xs">
                        <div class="col-sm-6"><strong>Тест</strong></div>
                        <div class="col-sm-2"><strong>Материал</strong></div>
                        <div class="col-sm-2"><strong>Срок, дней</strong></div>
                        <div class="col-sm-2 text-right"><strong>Цена</strong></div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Альфа-1-антитрипсин">Альфа-1-антитрипсин</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">900</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Аланинаминотрансфераза (АЛТ)">Аланинаминотрансфераза (АЛТ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Аспартатаминотрансфераза (АСТ)">Аспартатаминотрансфераза (АСТ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Щелочная фосфатаза (ЩФ)">Щелочная фосфатаза (ЩФ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Гаммаглютамилтрансфераза (ГГТП)">Гаммаглютамилтрансфераза (ГГТП)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                    <div class="row row--multiline">
                        <div data-title="Тест" class="col-sm-6"><a href="#" title="Лактатдегидрогеназа (ЛДГ)">Лактатдегидрогеназа (ЛДГ)</a></div>
                        <div data-title="Материал" class="col-sm-2">сыв.</div>
                        <div data-title="Срок, дней" class="col-sm-2">2</div>
                        <div data-title="Цена" class="col-sm-2 text-right">840</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    @extends('app.layouts.footer')

    @section('content')
<!--Only this page's scripts-->

<!---->
@endsection
