@include('app.layouts.header');

<section>
    <div class="container container--sm">
        <ul class="breadcrumbs">
            <li><a href="/" title="Главная">Главная</a></li>
            <li><span>{{$service->name}}</span></li>
        </ul>
        <article class="article">
            <h1 class="title-primary">{{$service->name}}</h1>
{{--            <div class="row row--multiline">--}}
{{--                <div class="col-sm-4">--}}
{{--                    <div class="billet">--}}
{{--                        <strong>Материал:</strong> сыворотка--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-4">--}}
{{--                    <div class="billet">--}}
{{--                        <strong>Срок, дней:</strong> 2--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-4">--}}
{{--                    <div class="billet">--}}
{{--                        <strong>Цена:</strong> 900--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <br>
{{--            <p class="annotation">--}}
{{--                Дефицит альфа-1-антитрипсина – это врожденная нехватка ингибитора легочных протеаз альфа-1-антитрипсина, которая приводит к усиленной, обусловленной протеазами, деструкции тканей легких и развитию эмфиземы у взрослых.--}}
{{--            </p>--}}
            <div class="text">
                <p>{{$service->description}}</p>
            </div>
        </article>
        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
    </div>
</section>

    @extends('app.layouts.footer')

    @section('content')
<!--Only this page's scripts-->

<!---->
@endsection
