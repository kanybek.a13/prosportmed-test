@component('mail::message')
# Заявка

Новая заявка.

Имя: {{$data['name']}}
<br>
Номер телефона: {{$data['phone']}}
<br>
Почта: {{$data['email']}}

{{--{{request('email')}}--}}
@component('mail::button', ['url' => ''])
    Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

