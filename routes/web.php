<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AidarController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\EquipmentController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\admin\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('app.index');
});

Route::post('/contact', [ContactController::class, 'send']);

Route::get('/doctors', [DoctorController::class, 'index']);
Route::get('/doctor/{doctor}', [DoctorController::class, 'show']);

Route::get('/article', [ArticleController::class, 'index']);

Route::get('/services', [ServiceController::class, 'index']);
Route::get('/service/{service}', [ServiceController::class, 'show']);

Route::get('/equipments', [EquipmentController::class, 'index']);
Route::get('/equipment/{equipment}', [EquipmentController::class, 'show']);




Route::get('/admin/login', [AdminController::class, 'login'])->name('admin.login');
Route::get('/admin/index', [AdminController::class, 'index'])->middleware('auth');

Route::get('/admin/users', [\App\Http\Controllers\admin\UserController::class, 'index'])->middleware('auth');
Route::get('/admin/new/user', [\App\Http\Controllers\admin\UserController::class, 'new'])->middleware('auth');
Route::get('/admin/user/{user}', [\App\Http\Controllers\admin\UserController::class, 'show'])->middleware('auth');
Route::get('/admin/user/edit/{user}', [\App\Http\Controllers\admin\UserController::class, 'edit'])->middleware('auth');
Route::get('/admin/user/delete/{user}', [\App\Http\Controllers\admin\UserController::class, 'delete'])->middleware('auth');
Route::put('/admin/user', [\App\Http\Controllers\admin\UserController::class, 'update'])->middleware('auth');
Route::post('/admin/user', [\App\Http\Controllers\admin\UserController::class, 'store'])->middleware('auth');


Route::get('/admin/doctors', [\App\Http\Controllers\admin\DoctorController::class, 'index'])->middleware('auth');
Route::get('/admin/new/doctor', [\App\Http\Controllers\admin\DoctorController::class, 'new'])->middleware('auth');
Route::get('/admin/doctor/{doctor}', [\App\Http\Controllers\admin\DoctorController::class, 'show'])->middleware('auth');
Route::get('/admin/doctor/edit/{doctor}', [\App\Http\Controllers\admin\DoctorController::class, 'edit'])->middleware('auth');
Route::get('/admin/doctor/delete/{doctor}', [\App\Http\Controllers\admin\DoctorController::class, 'delete'])->middleware('auth');
Route::put('/admin/doctor', [\App\Http\Controllers\admin\DoctorController::class, 'update'])->middleware('auth');
Route::post('/admin/doctor', [\App\Http\Controllers\admin\DoctorController::class, 'store'])->middleware('auth');


Route::get('/admin/equipments', [\App\Http\Controllers\admin\EquipmentController::class, 'index'])->middleware('auth');
Route::get('/admin/new/equipment', [\App\Http\Controllers\admin\EquipmentController::class, 'new'])->middleware('auth');
Route::get('/admin/equipment/{equipment}', [\App\Http\Controllers\admin\EquipmentController::class, 'show'])->middleware('auth');
Route::get('/admin/equipment/edit/{equipment}', [\App\Http\Controllers\admin\EquipmentController::class, 'edit'])->middleware('auth');
Route::get('/admin/equipment/delete/{equipment}', [\App\Http\Controllers\admin\EquipmentController::class, 'delete'])->middleware('auth');
Route::put('/admin/equipment', [\App\Http\Controllers\admin\EquipmentController::class, 'update'])->middleware('auth');
Route::post('/admin/equipment', [\App\Http\Controllers\admin\EquipmentController::class, 'store'])->middleware('auth');


Route::get('/admin/requests', [\App\Http\Controllers\admin\RequestController::class, 'index'])->middleware('auth');
Route::get('/admin/request/delete/{request}', [\App\Http\Controllers\admin\RequestController::class, 'delete'])->middleware('auth');


Route::get('/aidar/users', [AidarController::class, 'users']);
Route::get('/aidar/package-services', [AidarController::class, 'packageServices']);
Route::get('/aidar/create-admin', [AidarController::class, 'createAdmin']);
Route::get('/aidar/create-moderator', [AidarController::class, 'createModerator']);
Route::get('/aidar/create-content-manager', [AidarController::class, 'createContentManager']);

